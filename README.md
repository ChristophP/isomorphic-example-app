# Isomorphic Example App

This app will prerender a React Component on the server. The same components are also sent to the
client to handle the further user interaction.

1. The server will generate the initial HTML from the react components
2. On the client the React instance in the browser will notice that the View is already render
and do nothing else except adding of events.
3. When some user interaction happens the React instance will rerender the view depending on the input
and thus continue in the browser where the server left off.

This has the benefit of a fast first render. Because the view is already done without the need to fetch javascript first.
Also this makes it possible to use single page apps on and still have good search engine indexing.

This app uses Node.JS and Browser JS and is powered by this:

- ES6 and JSX with babel(requires node >=6.0.0)
- Express Server
- browserify for bundling

## To run the app

Install the dependencies.

`$ npm install`

Bundle up the javascript and fire up the server

`$ npm start`

Point your browser to `http://localhost:3000`

## To develop

Rebundle the react components on the fly.

`$ npm run watch`


