// START THE EXPRESS SERVER

// use babel to transform required files
require("babel-register");

const React = require("react"),
    ReactDOMServer = require('react-dom/server'),
    App = require("./src/components/app.jsx").default,
    express = require("express");

// create the server
const server = express();

// config
server.set('views', './views');
server.set('view engine', 'pug');

// start server
server.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

// add routes
server.get('/', function (req, res) {
  const markup = ReactDOMServer.renderToString(
      React.createElement(App)
  );

  res.render("index", {
    markup,
  });
});

// static files and assets
server.use(express.static('public'));
