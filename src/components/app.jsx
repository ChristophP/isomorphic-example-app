import React from 'react';
import Search from './search.jsx';

class App extends React.Component {
  render() {
    return (
        <Search />
    );
  }
};

export default App;
