import React from 'react';

class Search extends React.Component {
  constructor() {
    super();
    this.state = {
      search: ""
    };
  }

  changeSearch(event) {
    const text = event.target.value;

    this.setState({
      search: text
    });
  }

  render() {
    return (
        <div className="search-component">
          <input type="text" onChange={this.changeSearch.bind(this)}/>
          <span>You are searching for: {this.state.search}</span>
        </div>
    );
  }
};

export default Search;