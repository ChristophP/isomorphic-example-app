import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app.jsx';

if (typeof window !== "undefined") {
  window.onload = function() {
    // render element into same container as the server rendered view
    ReactDOM.render(React.createElement(App), document.getElementById('container'));
  };
}
